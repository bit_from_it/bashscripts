#!/bin/bash

# A script to install the basic tools after a fresh Linux installation.
# Author: milia 06/04/16

# First make your self a sudoer

# Add Greek keyboard layout

# Creating a dir to keep all deb files in.
mkdir installedPackages
cd installedPackages

# Linux stuff
sudo apt-get install -y aptitude curl htop

# Updating and upgrading
sudo aptitude update && sudo aptitude upgrade -y

# Communication/Internet progs
communication="openssh filezilla irssi xchat chromium-browser etherape"

skype="http://get.skype.com/go/getskype-linux-beta-ubuntu-64"
wget $skype
mv getskype-linux-beta-ubuntu-64 skype64.deb
echo "Installing dependencies"
sudo aptitude install -y libqt4-dbus libqt4-network libqt4-xml libqtcore4 libqtgui4 libqtwebkit4 libstdc++6 libxss1 libxv1 libssl1.0.0 libpulse0 libasound2-plugins
echo "Installing Skype"
sudo dpkg -i skype64.deb
echo "Installing the shit that break it"
sudo apt-get upgrade -f

viber="http://download.cdn.viber.com/cdn/desktop/Linux/viber.deb"
wget $viber
echo "Installing Viber"
sudo dpkg -i viber.deb
echo "Fixing Viber shit"
sudo apt-get upgrade -f 
torrents="torrent-search"

# Programming
curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash - # To install node
prog="mit-scheme gcc g++ gfortran nodejs monodevelop virtualenv git clang"
sudo aptitude install -y $prog

#Scientific software/math
scisoft="R rstudio weka python2.7 python3 julia g++ swi-prolog"

curl "https://bootstrap.pypa.io/get-pip.py" -o "get-pip.py"
# forgot this one:
chmod +x get-pip.py
sudo -H ./get-pip.py # didn't work
cd ..
mkdir venvs
cd venvs
virtualenv -p /usr/bin/python2.7 pynumerics
source pynumerics/bin/activate
sudo aptitude  install -y git python-pip python-virtualenv python3-dev python3-numpy python3-scipy python3-pyqt4 python-qt4-dev python3-sip-dev libqt4-dev #for Orange
sudo aptitude install -y build-essential gfortran libatlas-base-dev python-pip python-dev
echo "Installing python libraries with pip"
pip install numpy scipy pandas jupyter scikit-learn spyder matplotlib Orange ipython sympy
deactivate
cd ..
cd installedPackages

# Office
office="libreoffice djview djview4 okular texmaker texworks xchm xpdf calibre xournal"

mendeley="https://www.mendeley.com/repositories/ubuntu/stable/amd64/mendeleydesktop-latest"
wget $mendeley
mv mendeleydesktop-latest mendeley.deb
echo "Installing mendeley"
sudo dpkg -i mendeley.deb

# Audio/Video
audiovideo="vlc cheese shotwell audacity guvcview"

# Graphics
graphics="gimp imagemagick inkscape"

# Educational software
edu="geogebra stellarium celestia"

# Cloud services
mega="https://mega.nz/linux/MEGAsync/xUbuntu_16.04/amd64/megasync-xUbuntu_16.04_amd64.deb" # mega etc
wget $mega
mv megasync-xUbuntu_16.04_amd64.deb megasync.deb
sudo dpkg -i megasync.deb
sudo apt-get upgrade -f
cloud = ""

# Freelancing (upwork, guru)
freelancing =""

# Editors
editors="vim gvim pluma gedit"

sublime="https://download.sublimetext.com/sublime-text_build-3126_amd64.deb"
wget  $sublime
echo "Installing sublime"
sudo dpkg -i sublime-text_build-3126_amd64.deb
sudo apt-get upgrade -f

# Security
security="kleopatra onionshare"
torbrowser=""

# Virtualization/Emulation
virtual=""

# Productivity
product="tomboy onboard dclock"

# Games
games="xboard eboard gnuchess"

# Windows Managers
wm="xmonad"

# Boinc
#boinc="https://boinc.berkeley.edu/dl/boinc_7.2.42_x86_64-pc-linux-gnu.sh"
#wget $boinc
#chmod +x boinc_7.2.42_x86_64-pc-linux-gnu.sh
#echo "Installing Boinc"
#./boinc_7.2.42_x86_64-pc-linux-gnu.sh

# Installing rest of the programs
sudo aptitude install -y $games $product $virtual $security $editors $cloud $edu $graphics $audiovideo $office $scisoft $communication $wm

# Keeping a record of the programs installed

# Deleting downloaded all files and the directory
#rm *
#cd ..
#rmdir installedPackages
